﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using System;

public class ControllerHandler : MonoBehaviour
{
	public AudioClip menuButton;
	//private AudioSource audio;

    //menu objects
    private GameObject thisMainMenu;
    private MenuManager scriptMainMenu;

    //game menu manager objects for controller mapping
    private GameObject GameMenuManager;
    private GameManager script;

    //list of buttons
    public struct ActiveButtons
    {
        public Button button;
        public bool isActive;

        public ActiveButtons(Button b, bool k)
        {
            button = b;
            isActive = k;

        }

    }

    

    //struct
    private ActiveButtons themBs;

    //main menu -> done
    public Button Mstart;
    public Button Mselect;
    public Button Msettings;
    public Button Mexit;
    private List<ActiveButtons> MainButtons;
    private bool mainmenu;
   

    //settings menu -> done
    public Button Scontrol;
    public Button Ssound;
    public Button Saccount;
    public Button Sexit;
    private List<ActiveButtons> SettingsButtons;
    private bool settingsmenu;
   

    //hangar -> done
    public Button Hswitch;
    public Button Hexit;
    private List<ActiveButtons> HangarButtons;
    public bool hangarmenu;
  

    //mode menu ->done
    public Button ModeFFA;
    public Button ModeTD;
    public Button ModeCancel;
    public Button ModeSelect;
    private List<ActiveButtons> ModeButtons;
    public bool modemenu;
  
    //matchmaking menu -> done
    public Button MMcreate;
    public Button MMback;
    private bool matchmenu;
   
    
    ///room menu -> FFA
    public Button RMready;
    public Button RMlaunch;
    public Button RMback;
    public Button RMswitch;
    private List<ActiveButtons> RoomButtons;
    public bool roommenu;

    //room menu -> TD
    public Button RMTready;
    public Button RMTlaunch;
    public Button RMTback;
    public Button RMTred;
    public Button RMTblue;
    public Button RMTswitch;
    private List<ActiveButtons> RoomButtonsTD;
    public bool roommenuTD;

    //controls menu
    public Button Cleft;
    public Button Cright;
    public Button Cdown;
    public Button Cforward;
    public Button Cweapon1;
    public Button Cweapon2;
    public Button Cmenuselect;
    public Button Cmenuback;
    public Button CmenuTUP;
    public Button CmenuTDOWN;
    public Button Cback;
    public Button Csettings;
    public Button Cboost;
    private bool controlmenu;
    private List<ActiveButtons> ControlButtons;

    //sound menu
    public Button Soundback;
    private bool soundmenu;

    //account menu
    public Button AccountBack;
    private bool accountmenu;

    //for axis movements smoothness
    private float minimal;
    private float maximal;
    private int count;

  


    // Use this for initialization
    void Start()
    {
		//audio = GetComponent<AudioSource>();
       // audio.clip = menuButton;

        //main menu
        thisMainMenu = GameObject.Find("Canvas");
        scriptMainMenu = thisMainMenu.GetComponent<MenuManager>();

        //game manager
        GameMenuManager = GameObject.Find("GameMenuManager");
        script = GameMenuManager.GetComponent<GameManager>();

        //inits
        count = 0;
        minimal = 0;
        maximal = 1;
        mainmenu = false;
        settingsmenu = false;
        hangarmenu = false;
        modemenu = false;
        matchmenu = false;
        roommenu = false;
        controlmenu = false;
        accountmenu = false;
        soundmenu = false;
        roommenuTD = false;

       
        //load the lists with buttons
        loadLists();
        


    }
    public void loadLists()
    {
        //main menu
        MainButtons = new System.Collections.Generic.List<ActiveButtons>();
        //start
        themBs.button = Mstart;
        themBs.isActive = false;
        MainButtons.Add(themBs);
        //hangar
        themBs.button = Mselect;
        themBs.isActive = false;
        MainButtons.Add(themBs);
        //system
        themBs.button = Msettings;
        themBs.isActive = false;
        MainButtons.Add(themBs);
        //escape
        themBs.button = Mexit;
        themBs.isActive = false;
        MainButtons.Add(themBs);

        //settings menu
        //adds the buttons to the list
        SettingsButtons = new System.Collections.Generic.List<ActiveButtons>();
        //controls
        themBs.button = Scontrol;
        themBs.isActive = false;
        SettingsButtons.Add(themBs);
        //sound
        themBs.button = Ssound;
        themBs.isActive = false;
        SettingsButtons.Add(themBs);
        //account
        themBs.button = Saccount;
        themBs.isActive = false;
        SettingsButtons.Add(themBs);
        //escape
        themBs.button = Sexit;
        themBs.isActive = false;
        SettingsButtons.Add(themBs);

        //room menu FFA
        RoomButtons = new System.Collections.Generic.List<ActiveButtons>();
        //switch
        themBs.button = RMswitch;
        themBs.isActive = false;
        RoomButtons.Add(themBs);
        //back
        themBs.button = RMback;
        themBs.isActive = false;
        RoomButtons.Add(themBs);
        //master launch
        themBs.button = RMlaunch;
        themBs.isActive = false;
        RoomButtons.Add(themBs);
        //readu
        themBs.button = RMready;
        themBs.isActive = false;
        RoomButtons.Add(themBs);
       
        ///room menu TD
        RoomButtonsTD = new System.Collections.Generic.List<ActiveButtons>();
        //red
        themBs.button = RMTred;
        themBs.isActive = false;
        RoomButtonsTD.Add(themBs);
        //blue
        themBs.button = RMTblue;
        themBs.isActive = false;
        RoomButtonsTD.Add(themBs);
        //back
        themBs.button = RMTback;
        themBs.isActive = false;
        RoomButtonsTD.Add(themBs);
        //master launch
        themBs.button = RMTlaunch;
        themBs.isActive = false;
        RoomButtonsTD.Add(themBs);
        //ready
        themBs.button = RMTready;
        themBs.isActive = false;
        RoomButtonsTD.Add(themBs);
        //switch
        themBs.button = RMTswitch;
        themBs.isActive = false;
        RoomButtonsTD.Add(themBs);

        ///hangar menu
        HangarButtons = new System.Collections.Generic.List<ActiveButtons>();
        //switch
        themBs.button = Hswitch;
        themBs.isActive = false;
        HangarButtons.Add(themBs);
        //exit
        themBs.button = Hexit;
        themBs.isActive = false;
        HangarButtons.Add(themBs);

        //mode menu
        ModeButtons  = new System.Collections.Generic.List<ActiveButtons>();
        //ffa
        themBs.button = ModeFFA;
        themBs.isActive = false;
        ModeButtons.Add(themBs);
        //td
        themBs.button = ModeTD;
        themBs.isActive = false;
        ModeButtons.Add(themBs);
        //select
        themBs.button = ModeSelect;
        themBs.isActive = false;
        ModeButtons.Add(themBs);
        //cancel
        themBs.button = ModeCancel;
        themBs.isActive = false;
        ModeButtons.Add(themBs);

        //controls menu
        ControlButtons = new System.Collections.Generic.List<ActiveButtons>();
        //left
        themBs.button = Cleft;
        themBs.isActive = false;
        ControlButtons.Add(themBs);
        //right
        themBs.button = Cright;
        themBs.isActive = false;
        ControlButtons.Add(themBs);
        //forward
        themBs.button = Cdown;
        themBs.isActive = false;
        ControlButtons.Add(themBs);
        //back
        themBs.button = Cforward;
        themBs.isActive = false;
        ControlButtons.Add(themBs);
        //weapon1
        themBs.button = Cweapon1;
        themBs.isActive = false;
        ControlButtons.Add(themBs);
        //weapon2
        themBs.button = Cweapon2;
        themBs.isActive = false;
        ControlButtons.Add(themBs);
        //boost
        themBs.button = Cboost;
        themBs.isActive = false;
        ControlButtons.Add(themBs);
        //settings
        themBs.button = Csettings;
        themBs.isActive = false;
        ControlButtons.Add(themBs);
        //menuback
        themBs.button = Cmenuback;
        themBs.isActive = false;
        ControlButtons.Add(themBs);
        //menuselect
        themBs.button = Cmenuselect;
        themBs.isActive = false;
        ControlButtons.Add(themBs);
        //menutraverseup
        themBs.button = CmenuTUP;
        themBs.isActive = false;
        ControlButtons.Add(themBs);
        //menutraversdown
        themBs.button = CmenuTDOWN;
        themBs.isActive = false;
        ControlButtons.Add(themBs);
        //back
        themBs.button = Cback;
        themBs.isActive = false;
        ControlButtons.Add(themBs);


      


    }
    // Update is called once per frame
    void Update()
    {

        checkCoreRoutine();
        checkwhichScript();
        checkwhichResponse();
        makeSure();

    }
    public void checkCoreRoutine()
    {


        if (mainmenu == false)
        {
            CheckOpen();
        }
        else if(settingsmenu == false)
        {
            CheckOpen();
        }
        else if(hangarmenu == false)
        {
            CheckOpen();
        }
        else if(modemenu == false)
        {
            CheckOpen();
        }
        else if(matchmenu == false)
        {
            CheckOpen();
        }
        else if(roommenu == false)
        {
            CheckOpen();
        }
        else if(roommenuTD == false)
        {
            CheckOpen();
        }
        else if(controlmenu == false)
        {
            CheckOpen();
        }
        else if(soundmenu == false)
        {
            CheckOpen();
        }
        else if(accountmenu == false)
        {
            CheckOpen();
        }

    }
    public void CheckOpen()
    {


        if (scriptMainMenu != null)
        {
            string stringzz = scriptMainMenu.returnOpen();

            if (stringzz == "MainMenu")
            {
                StartCoroutine(CheckStatusMainMenu());
            }
            else if(stringzz == "SettingsMenu")
            {
                StartCoroutine(CheckStatusSettings());
            }
            else if(stringzz == "HangarClankMenu")
            {
                StartCoroutine(CheckStatusHangarMenu());
            }
            else if(stringzz == "GM")
            {
                StartCoroutine(CheckStatusModeMenu());
            }
            else if(stringzz == "MatchMakingMenu")
            {
                StartCoroutine(CheckStatusMatchMenu());
            }
            else if(stringzz == "RoomMenuFFA")
            {
                StartCoroutine(CheckStatusRoomMenu());
            }
            else if(stringzz == "RoomMenuTD")
            {
                StartCoroutine(CheckStatusROOMmenuTD());
            }
            else if(stringzz == "ControlsMenu")
            {
                StartCoroutine(CheckStatusControlRoomMenu());
            }
            else if(stringzz == "AccountManagerMenu")
            {
                StartCoroutine(CheckStatusAccountMenu());
            }
            else if(stringzz == "SoundControlMenu")
            {
                StartCoroutine(CheckStatusSoundMenu());
            }


        }



    }
    IEnumerator CheckStatusSoundMenu()
    {
        yield return new WaitForSeconds(1.0f);

        soundmenu = true;
        //count = 0;

    }
    IEnumerator CheckStatusAccountMenu()
    {
        yield return new WaitForSeconds(1.0f);

        accountmenu = true;
        //count = 0;

    }
    IEnumerator CheckStatusSettings()
    {
        yield return new WaitForSeconds(1.0f);

        settingsmenu = true;
        //count = 0;

    }
    IEnumerator CheckStatusMainMenu()
    {
        yield return new WaitForSeconds(1.0f);

        mainmenu = true;
        //count = 0;

    }
    IEnumerator CheckStatusHangarMenu()
    {
        yield return new WaitForSeconds(1.0f);

        hangarmenu = true;
        //count = 0;

    }
    IEnumerator CheckStatusModeMenu()
    {
        yield return new WaitForSeconds(1.0f);

        modemenu = true;
        //count = 0;

    }
    IEnumerator CheckStatusMatchMenu()
    {
        yield return new WaitForSeconds(1.0f);

        matchmenu = true;
        //count = 0;

    }
    IEnumerator CheckStatusRoomMenu()
    {
        //FFA
        yield return new WaitForSeconds(1.0f);

        roommenu = true;
        //count = 0;

    }
    IEnumerator CheckStatusControlRoomMenu()
    {
        yield return new WaitForSeconds(1.0f);

        controlmenu = true;
        //count = 0;

    }
    IEnumerator CheckStatusROOMmenuTD()
    {
        yield return new WaitForSeconds(1.0f);

        roommenuTD = true;
        //count = 0;

    }

    public void checkwhichScript()
    {

        if (scriptMainMenu != null)
        {
            string stringzz = scriptMainMenu.returnOpen();

            if (stringzz != "MainMenu")
            {
                mainmenu = false;
            }
            else if(stringzz != "SettingsMenu")
            {
                settingsmenu = false;
            }
            else if(stringzz != "HangarClankMenu")
            {
                hangarmenu = false;
            }
            else if(stringzz != "GM")
            {
                 modemenu = false;
            }
            else if(stringzz != "MatchMakingMenu")
            {
                matchmenu = false;
            }
            else if(stringzz != "RoomMenuFFA")
            {
                roommenu = false;
            }
            else if(stringzz != "RoomMenuTD")
            {
                roommenuTD = false;
            }
            else if(stringzz != "ControlsMenu")
            {
                controlmenu = false;
            }
            else if(stringzz != "SoundControlMenu")
            {
                soundmenu = false;
            }
            else if(stringzz != "AccountManagerMenu")
            {
                accountmenu = false;
            }


        }

    }
    public void makeSure()
    {
        if (scriptMainMenu != null)
        {
            string stringzz = scriptMainMenu.returnOpen();

            if (stringzz == "MainMenu")
            {
                mainmenu = true;
                settingsmenu = false;
                hangarmenu = false;
                modemenu = false;
                matchmenu = false;
                roommenu = false;
                controlmenu = false;
                accountmenu = false;
                soundmenu = false;
                roommenuTD = false;
            }
            else if (stringzz == "SettingsMenu")
            {
                settingsmenu = true;
                mainmenu = false;
                hangarmenu = false;
                modemenu = false;
                matchmenu = false;
                roommenu = false;
                controlmenu = false;
                accountmenu = false;
                soundmenu = false;
                roommenuTD = false;
            }
            else if (stringzz == "HangarClankMenu")
            {
                hangarmenu = true;
                mainmenu = false;
                settingsmenu = false;
                modemenu = false;
                matchmenu = false;
                roommenu = false;
                controlmenu = false;
                accountmenu = false;
                soundmenu = false;
                roommenuTD = false;
            }
            else if (stringzz == "GM")
            {
                modemenu = true;
                hangarmenu = false;
                mainmenu = false;
                settingsmenu = false;
                matchmenu = false;
                roommenu = false;
                controlmenu = false;
                accountmenu = false;
                soundmenu = false;
                roommenuTD = false;

            }
            else if (stringzz == "MatchMakingMenu")
            {
                matchmenu = true;
                mainmenu = false;
                settingsmenu = false;
                hangarmenu = false;
                modemenu = false;
                roommenu = false;
                controlmenu = false;
                accountmenu = false;
                soundmenu = false;
                roommenuTD = false;


            }
            else if(stringzz == "RoomMenuFFA")
            {
                roommenu = true;
                matchmenu = false;
                mainmenu = false;
                settingsmenu = false;
                hangarmenu = false;
                modemenu = false;
                controlmenu = false;
                accountmenu = false;
                soundmenu = false;
                roommenuTD = false;
            }
            else if (stringzz == "RoomMenuTD")
            {
                roommenuTD = true;
                matchmenu = false;
                mainmenu = false;
                settingsmenu = false;
                hangarmenu = false;
                modemenu = false;
                controlmenu = false;
                accountmenu = false;
                soundmenu = false;
                roommenu = false;
            }
            else if(stringzz == "ControlsMenu")
            {
                controlmenu = true;
                roommenu = false;
                matchmenu = false;
                mainmenu = false;
                settingsmenu = false;
                hangarmenu = false;
                modemenu = false;
                accountmenu = false;
                soundmenu = false;
                roommenuTD = false;
              

            }
            else if (stringzz == "SoundControlMenu")
            {
                soundmenu = true;
                controlmenu = false;
                roommenu = false;
                matchmenu = false;
                mainmenu = false;
                settingsmenu = false;
                hangarmenu = false;
                modemenu = false;
                accountmenu = false;
                roommenuTD = false;
                


            }
            else if (stringzz == "AccountManagerMenu")
            {
                accountmenu = true;
                soundmenu = false;
                controlmenu = false;
                roommenu = false;
                matchmenu = false;
                mainmenu = false;
                settingsmenu = false;
                hangarmenu = false;
                modemenu = false;
                roommenuTD = false;


            }
        }

    }
    

    public void checkwhichResponse()
    {

        //check which menu should be active
        if (mainmenu == true)
        {
            makeResponsive(MainButtons);
            //Debug.Log("Main Menu is true");
        }
        else if (settingsmenu == true)
        {
            makeResponsive(SettingsButtons);
            //Debug.Log("Settings is true");
        }
        else if (hangarmenu == true)
        {
            makeResponsive(HangarButtons);
            //Debug.Log("Hangar menu is true");
        }
        else if(modemenu == true)
        {
            makeResponsive(ModeButtons);
            //Debug.Log("Mode menu is true");
        }
        else if(matchmenu == true)
        {
            checkMatchMaking();
            //Debug.Log("match menu is true");
        }
        else if(roommenu == true)
        {
            checkRoomMenuMaking();
            //Debug.Log("room menu is true");
        }
        else if(roommenuTD == true)
        {
            makeResponsive(RoomButtonsTD);
        }
        else if(controlmenu == true)
        {
            makeResponsive(ControlButtons);
            //Debug.Log("control menu is true");
        }
        else if(soundmenu == true)
        {
            checkSoundMenu();
        }
        else if(accountmenu == true)
        {
            checkAccountMenu();
        }


    }

    //make buttons active and responsive
    public void makeActive(List<ActiveButtons> theButton)
    {
        
            for (int i = 0; i < theButton.Count; i++)
            {
                if (theButton[count].button != null)
                {
                    if (count != i)
                    {
                        theButton[i] = new ActiveButtons(theButton[i].button, false);
                    }
                    else
                    {
                        theButton[count] = new ActiveButtons(theButton[count].button, true);
                        theButton[count].button.Select();
                    }
                }



            }
     


    }


    //make responsive depending on the open scene
    public void makeResponsive(List<ActiveButtons> theButton)
    {
        
        makeActive(theButton);

        //Debug.Log("the button count is " + theButton.Count);
        //Debug.Log("Count is " + count);

        //your down traverse
        if (Input.GetKeyDown((KeyCode)script.userControlsKeyboard.MtraverseDown.keyCode))
        {

            count += 1;
            AudioSource.PlayClipAtPoint(menuButton, this.transform.position);

            if (count > theButton.Count - 1)
            {
                count = 0;
            }

        }

        //your down traverse joystick
        if ((Input.GetAxis("HUDvertical") == 1))
        {

            minimal += 0.17f;

            if (minimal >= 1.00f)
            {
                count += 1;
                AudioSource.PlayClipAtPoint(menuButton, this.transform.position);
                minimal = 0;
            }

            if (count > theButton.Count - 1)
            {
                count = 0;
            }

        }



        //your up traverse
        if (Input.GetKeyDown((KeyCode)script.userControlsKeyboard.MtraverseUp.keyCode))
        {

            count -= 1;
            AudioSource.PlayClipAtPoint(menuButton, this.transform.position);


            if (count < 0)
            {
                count = theButton.Count - 1;
            }

        }



        //up joystick traverse
        if (Input.GetAxis("HUDvertical") == -1)
        {
            maximal -= 0.17f;

            if (maximal <= 0.0f)
            {
                maximal = 1;
                count -= 1;
                AudioSource.PlayClipAtPoint(menuButton, this.transform.position);

            }

            if (count < 0)
            {
                count = theButton.Count - 1;
            }

        }

        //control menu select
       // if ((controlmenu == true && Input.GetKeyDown((KeyCode)script.userControlsKeyboard.Mselectmenu.keyCode)) ||
          //  (controlmenu == true && Input.GetKeyDown((KeyCode)KeyCode.Return)) || (controlmenu == true && 
            //Input.GetKeyDown((KeyCode)KeyCode.Mouse0)) )
       // {
            //for (int i = 0; i < theButton.Count; i++)
            //{
                //if (theButton[i].isActive == true)
                //{
                    //Debug.Log(theButton[i].button + " is being selected");
                   // theButton[i].button.onClick.Invoke();
                   // count = 0;
                   // AudioSource.PlayClipAtPoint(menuButton, this.transform.position);
               // }

          //  }

       // }

        if (Input.GetKeyDown((KeyCode)script.userControlsKeyboard.ACMselectmenu.keyCode))
        {
            //Debug.Log("count is " + count);
            for (int i = 0; i < theButton.Count; i++)
            {


                if (theButton[i].isActive == true)
                {
                    //Debug.Log(theButton[i].button + " is being selected");
                    theButton[i].button.onClick.Invoke();
                    count = 0;
                }

            }

        }





        //select
        if (Input.GetKeyDown((KeyCode)script.userControlsKeyboard.Mselectmenu.keyCode)) 
        {
            for (int i = 0; i < theButton.Count; i++)
            {
                if (theButton[i].isActive == true)
                {
                    //Debug.Log(theButton[i].button + " is being selected");
                    
                    theButton[i].button.onClick.Invoke();
                    count = 0;
                    AudioSource.PlayClipAtPoint(menuButton, this.transform.position);
                }

            }

        }




        

        if (Input.GetKeyDown((KeyCode)script.userControlsKeyboard.ACMselectmenu.keyCode))
        {
            //Debug.Log("count is " + count);
            for (int i = 0; i < theButton.Count; i++)
            {


                if (theButton[i].isActive == true)
                {
                    //Debug.Log(theButton[i].button + " is being selected");
                    theButton[i].button.onClick.Invoke();
                    count = 0;
                    AudioSource.PlayClipAtPoint(menuButton, this.transform.position);
                }

            }

        }
    

        EscapeButton();


    }

    public void EscapeButton()
    {
        if (mainmenu == true)
        {
            

                if (Input.GetKeyDown((KeyCode)script.userControlsKeyboard.Mbackmenu.keyCode)
                || Input.GetKeyDown((KeyCode)script.userControlsKeyboard.ACMbackmenu.keyCode))
                {

                    Mexit.onClick.Invoke();
                    count = 0;
                    AudioSource.PlayClipAtPoint(menuButton, this.transform.position);

                }
            }
            else if (settingsmenu == true)
            {
                if (Sexit != null)
                {
                    if (Input.GetKeyDown((KeyCode)script.userControlsKeyboard.Mbackmenu.keyCode)
                   || Input.GetKeyDown((KeyCode)script.userControlsKeyboard.ACMbackmenu.keyCode))
                    {

                        Sexit.onClick.Invoke();
                        count = 0;
                        AudioSource.PlayClipAtPoint(menuButton, this.transform.position);
                    }
                }
            }
            else if (hangarmenu == true)
            {
                if (Hexit != null)
                {
                    if (Input.GetKeyDown((KeyCode)script.userControlsKeyboard.Mbackmenu.keyCode)
                   || Input.GetKeyDown((KeyCode)script.userControlsKeyboard.ACMbackmenu.keyCode))
                    {

                        Hexit.onClick.Invoke();
                        count = 0;
                        AudioSource.PlayClipAtPoint(menuButton, this.transform.position);
                    }


                    }

             }
        else if(modemenu == true)
        {
            if (ModeCancel != null)
            {
                if (Input.GetKeyDown((KeyCode)script.userControlsKeyboard.Mbackmenu.keyCode)
               || Input.GetKeyDown((KeyCode)script.userControlsKeyboard.ACMbackmenu.keyCode))
                {

                    ModeCancel.onClick.Invoke();
                    count = 0;
                    AudioSource.PlayClipAtPoint(menuButton, this.transform.position);
                }


            }

        }
        else if(controlmenu == true)
        {
            if(Cback != null)
            {

                if (Input.GetKeyDown((KeyCode)script.userControlsKeyboard.Mbackmenu.keyCode)
               || Input.GetKeyDown((KeyCode)script.userControlsKeyboard.ACMbackmenu.keyCode))
                {

                    Cback.onClick.Invoke();
                    count = 0;
                    AudioSource.PlayClipAtPoint(menuButton, this.transform.position);
                }



            }




        }
        else if (roommenuTD == true)
        {

            if (RMTback != null)
            {

                if (Input.GetKeyDown((KeyCode)script.userControlsKeyboard.Mbackmenu.keyCode)
               || Input.GetKeyDown((KeyCode)script.userControlsKeyboard.ACMbackmenu.keyCode))
                {

                    RMTback.onClick.Invoke();
                    count = 0;
                    AudioSource.PlayClipAtPoint(menuButton, this.transform.position);
                }


            }




        }
  


            


     }
    public void checkMatchMaking()
    {

        if (matchmenu == true)
        {

            if (MMback != null)
            {
                if (Input.GetKeyDown((KeyCode)script.userControlsKeyboard.Mbackmenu.keyCode)
               || Input.GetKeyDown((KeyCode)script.userControlsKeyboard.ACMbackmenu.keyCode))
                {

                    MMback.onClick.Invoke();
                    count = 0;
                    AudioSource.PlayClipAtPoint(menuButton, this.transform.position);
                }


            }

            if(MMcreate != null)
            {

                 if (Input.GetKeyDown((KeyCode)script.userControlsKeyboard.ACMselectmenu.keyCode))
                 {

                     MMcreate.onClick.Invoke();
                     count = 0;
                     AudioSource.PlayClipAtPoint(menuButton, this.transform.position);

                 }



            }





        }
    }
    //ffa
    public void checkRoomMenuMaking()
    {


        if(roommenu == true)
        {

            if(RMback != null)
            {

                if (Input.GetKeyDown((KeyCode)script.userControlsKeyboard.Mbackmenu.keyCode)
               || Input.GetKeyDown((KeyCode)script.userControlsKeyboard.ACMbackmenu.keyCode))
                {

                    RMback.onClick.Invoke();
                    count = 0;
                    AudioSource.PlayClipAtPoint(menuButton, this.transform.position);
                }


            }

            //master player launch
            if(RMlaunch != null && RMlaunch.IsActive() == true)
            {
                 if (PhotonNetwork.isMasterClient)
                 {
                     if (Input.GetKeyDown((KeyCode)script.userControlsKeyboard.ACMselectmenu.keyCode))
                     {

                         RMlaunch.onClick.Invoke();
                         count = 0;
                         AudioSource.PlayClipAtPoint(menuButton, this.transform.position);
                     }



                 }


            }



            //plagyer ready
            if (RMready != null && RMready.IsActive() == true)
            {
                if (!PhotonNetwork.isMasterClient)
                {
                    if (Input.GetKeyDown((KeyCode)script.userControlsKeyboard.ACMselectmenu.keyCode))
                    {

                        RMready.onClick.Invoke();
                        count = 0;
                        AudioSource.PlayClipAtPoint(menuButton, this.transform.position);
                    }



                }




            }









        }




    }
    public void checkAccountMenu()
    {
        if (AccountBack != null)
        {

            if (Input.GetKeyDown((KeyCode)script.userControlsKeyboard.Mbackmenu.keyCode)
           || Input.GetKeyDown((KeyCode)script.userControlsKeyboard.ACMbackmenu.keyCode))
            {

                AccountBack.onClick.Invoke();
                count = 0;
                AudioSource.PlayClipAtPoint(menuButton, this.transform.position);
            }


          

                if (Input.GetKeyDown((KeyCode)script.userControlsKeyboard.ACMselectmenu.keyCode))
                {

                    AccountBack.onClick.Invoke();
                    count = 0;
                    AudioSource.PlayClipAtPoint(menuButton, this.transform.position);

                }



            


        }



    }
    public void checkSoundMenu()
    {

        if (Soundback != null)
        {

            if (Input.GetKeyDown((KeyCode)script.userControlsKeyboard.Mbackmenu.keyCode)
           || Input.GetKeyDown((KeyCode)script.userControlsKeyboard.ACMbackmenu.keyCode))
            {

                Soundback.onClick.Invoke();
                count = 0;
                AudioSource.PlayClipAtPoint(menuButton, this.transform.position);
            }


        }


        if (Input.GetKeyDown((KeyCode)script.userControlsKeyboard.ACMselectmenu.keyCode))
        {

            Soundback.onClick.Invoke();
            count = 0;
            AudioSource.PlayClipAtPoint(menuButton, this.transform.position);

        }



    }
    public void detectPressedKeyOrButton()
    {
        foreach (KeyCode kcode in Enum.GetValues(typeof(KeyCode)))
        {
            if (Input.GetKeyDown(kcode))
                Debug.Log("--KeyCode down: " + kcode + " --");
        }
    }



    



}
